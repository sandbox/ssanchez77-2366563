/**
 * @file
 * HTML5 Colorpicker Settings.
 *
 * Copyright 2014, Simón Sánchez.
 * http://www.cinetica.co.cr/
 */

(function($) {
    Drupal.behaviors.html5ColorpickerSettings = {
        attach: function(context, settings) {

            var basePath = Drupal.settings.html5ColorpickerSettings.basePath;
            var initialSelector = Drupal.settings.html5ColorpickerSettings.initialSelector;
            var initialBorderRadius = Drupal.settings.html5ColorpickerSettings.initialBorderRadius;
            var initialBackgroundColor = Drupal.settings.html5ColorpickerSettings.initialBackgroundColor;
            var initialWidth = Drupal.settings.html5ColorpickerSettings.initialWidth;
            var initialHeight = Drupal.settings.html5ColorpickerSettings.initialHeight;

            $( "#edit-widget-style .fieldset-wrapper" ).after( "<div class='picker-sample-wrapper'></div>" );
            $( "#edit-widget-style .picker-sample-wrapper" ).append( "<div class='selector-sample'></div>" );

            var wrapperStyles = {
              width : initialWidth[0] + "px",
              height: initialHeight[0] + "px",
              borderRadius: initialBorderRadius[0] + "px",
              backgroundColor: "#" + initialBackgroundColor[0],
              left: $( "#edit-widget-style .fieldset-wrapper" ).width() + "px",
            };
            $( ".picker-sample-wrapper" ).css( wrapperStyles );
            var selectorStyles = {
              background: "url('" + chooseSelector(basePath[0], initialSelector[0]) + "') no-repeat scroll center center transparent",
            };
            $( ".selector-sample" ).css( selectorStyles );

            // Border radius changed
            $('#edit-html5-colorpicker-widget-border-radius').change(function() {
                var newWrapperStyles = {
                  borderRadius: $('#edit-html5-colorpicker-widget-border-radius').val() + "px",
                };
                $( ".picker-sample-wrapper" ).css( newWrapperStyles );
            });
            // Background color changed
            $('#edit-html5-colorpicker-background-color').change(function() {
                var colorPattern = /^([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/;
                if (colorPattern.test($('#edit-html5-colorpicker-background-color').val())) {
                    var newWrapperStyles = {
                      backgroundColor: "#" + $('#edit-html5-colorpicker-background-color').val(),
                    };
                    $( ".picker-sample-wrapper" ).css( newWrapperStyles );
                }
            });
            // Width changed
            $('#edit-html5-colorpicker-widget-picker-width').change(function() {
                var newWrapperStyles = {
                  width : $('#edit-html5-colorpicker-widget-picker-width').val() + "px",
                };
                $( ".picker-sample-wrapper" ).css( newWrapperStyles );
            });
            // Height changed
            $('#edit-html5-colorpicker-widget-picker-height').change(function() {
                var newWrapperStyles = {
                  height: $('#edit-html5-colorpicker-widget-picker-height').val() + "px",
                };
                $( ".picker-sample-wrapper" ).css( newWrapperStyles );
            });
            // Selector changed
            $('#edit-html5-colorpicker-selector').change(function() {
                // select new color selector
                var newSelectorStyles = {
                  background: "url('" + chooseSelector(basePath[0], $('#edit-html5-colorpicker-selector').val()) + "') no-repeat scroll center center transparent",
                };
                $( ".selector-sample" ).css( newSelectorStyles );

            });

        }
    };
    // auxiliar function
    function chooseSelector(path, index) {
        // choose desired color selector
        var imageSrc = path + '/images/colorwheel1.png';
        switch (index) {
            case '1':
                imageSrc = path + '/images/colorwheel2.png';
                break;
            case '2':
                imageSrc = path + '/images/colorwheel3.png';
                break;
            case '3':
                imageSrc = path + '/images/colorwheel4.png';
                break;
            case '4':
                imageSrc = path + '/images/colorwheel5.png';
                break;
            case '5':
                imageSrc = path + '/images/colorwheel6.png';
                break;
            case '6':
                imageSrc = path + '/images/colorwheel7.png';
                break;
            case '7':
                imageSrc = path + '/images/colorwheel8.png';
                break;
            case '8':
                imageSrc = path + '/images/colorpalette1.png';
                break;
            case '9':
                imageSrc = path + '/images/colorpalette2.png';
                break;
            case '10':
                imageSrc = path + '/images/colorpalette3.png';
                break;
            case '11':
                imageSrc = path + '/images/colorpalette4.png';
                break;
            case '12':
                imageSrc = path + '/images/colorpalette5.png';
                break;
            case '13':
                imageSrc = path + '/images/colorpalette6.png';
                break;
            case '14':
                imageSrc = path + '/images/colorpalette7.png';
                break;
            case '15':
                imageSrc = path + '/images/colorpalette8.png';
                break;
            case '16':
                imageSrc = path + '/images/colorspectrum1.png';
                break;
            case '17':
                imageSrc = path + '/images/colorspectrum2.png';
                break;
            case '18':
                imageSrc = path + '/images/colorspectrum3.png';
                break;
        }
        return imageSrc;
    }
})(jQuery);
