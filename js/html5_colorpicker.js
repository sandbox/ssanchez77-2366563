/**
 * @file
 * HTML5 Colorpicker.
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright 2012, Script Tutorials.
 * http://www.script-tutorials.com/
 *
 * Modified on 2014 by Simón Sánchez.
 * http://www.cinetica.co.cr/
 */

(function($) {
    Drupal.behaviors.html5Colorpicker = {
        attach: function(context, settings) {

            var arrayWrappers = new Array();
            var basePath = Drupal.settings.html5Colorpicker.basePath;
            var ids = Drupal.settings.html5Colorpicker.ids;
            var names = Drupal.settings.html5Colorpicker.names;
            var initialColors = Drupal.settings.html5Colorpicker.initialColors;
            var showDataField = Drupal.settings.html5Colorpicker.showDataField;

            // First we initialize some things for each entity
            for (var i = 0; i < Drupal.settings.html5Colorpicker.ids.length; i++) {

                // set initial values
                var containerId = "#html5-colorpicker-" + i;
                $(containerId + ' .rVal').val( hexToRgb(initialColors[i]).r );
                $(containerId + ' .gVal').val( hexToRgb(initialColors[i]).g );
                $(containerId + ' .bVal').val( hexToRgb(initialColors[i]).b );
                $(containerId + ' .rgbVal').val($(containerId + ' .rVal').val() + ',' + $(containerId + ' .gVal').val() + ',' + $(containerId + ' .bVal').val());
                $(containerId + ' .hexVal').val('#' + initialColors[i]);

                // hide the input element ?
                if (!(showDataField[0])) {
                    $("input[type='text'][name='" + names[i] + "']").css({"display" : "none"});
                }

                var innerWrapper = ids[i] + "-inner_wrapper";
                arrayWrappers.push(innerWrapper);
            }

            if ((context == document) || (context.context == $("form#article-node-form.node-form.node-article-form")[0])) {

                // next we use the list of IDs we just built and act upon each of them
                $(arrayWrappers).each(function() {

                    var bCanPreview = true; // can preview

                    // get the canvas element por each wrapper
                    var canvas = $('#' + this + ' canvas')[0];
                    var ctx = canvas.getContext('2d');

                    // drawing active image
                    var image = new Image();
                    image.onload = function () {
                        ctx.drawImage(image, canvas.width / 2 - image.width / 2, canvas.height / 2 - image.height / 2, image.width, image.height); // draw the image on the canvas
                    }

                    // select desired colorwheel
                    image.src = chooseSelector(basePath[0], $(canvas).attr('var'));

                    // preview button click
                    $('#' + this + ' .preview').click(function(e) {

                        var html5Colorpicker = $(this).siblings(".html5-colorpicker");
                        if (!($(this).hasClass('expanded'))) {
                            // Reload element value
                            currentColor = $(this).siblings("input[type='text']").val();

                            if (currentColor == '') currentColor = 'FFFFFF';

                            html5Colorpicker.find(".rVal").val( hexToRgb(currentColor).r );
                            html5Colorpicker.find(".gVal").val( hexToRgb(currentColor).g );
                            html5Colorpicker.find(".bVal").val( hexToRgb(currentColor).b );
                            html5Colorpicker.find(".rgbVal").val(html5Colorpicker.find(".rVal").val() + ',' + html5Colorpicker.find(".gVal").val() + ',' + html5Colorpicker.find(".bVal").val());
                            html5Colorpicker.find(".hexVal").val('#' + currentColor);
                        }

                        html5Colorpicker.fadeToggle("slow", "linear");
                        $(this).toggleClass('expanded');

                        bCanPreview = false;

                    });

                    // click event handler
                    $('#' + this + ' .html5-colorpicker' + ' .picker').click(function(e) {
                        bCanPreview = !bCanPreview;

                    });

                    // clear event handler
                    $('#' + this).parent().siblings(".html5-colorpicker-clear-button").click(function(e) {
                        var innerWrapper = $(this).siblings().find(".inner_wrapper");
                        innerWrapper.children("input[type='text']").val('');
                        $(this).siblings(".form-type-html5-colorpicker").find(".preview").removeAttr("style");
                    });

                    // mouse move handler
                    $('#' + this + ' .html5-colorpicker' + ' .picker').mousemove(function(e) {
                        if (bCanPreview) {
                            // get the html5Colorpicker element
                            var html5Colorpicker = $(this).parent();

                            // get coordinates of current position
                            var canvasOffset = $(canvas).offset();
                            var canvasX = Math.floor(e.pageX - canvasOffset.left);
                            var canvasY = Math.floor(e.pageY - canvasOffset.top);

                            // get current pixel
                            var imageData = ctx.getImageData(canvasX, canvasY, 1, 1);
                            var pixel = imageData.data;

                            // update preview color
                            var pixelColor = "rgb("+pixel[0]+", "+pixel[1]+", "+pixel[2]+")";
                            html5Colorpicker.siblings('.preview').css('backgroundColor', pixelColor);

                            // update controls
                            html5Colorpicker.find('.rVal').val(pixel[0]);
                            html5Colorpicker.find('.gVal').val(pixel[1]);
                            html5Colorpicker.find('.bVal').val(pixel[2]);
                            html5Colorpicker.find('.rgbVal').val(pixel[0]+','+pixel[1]+','+pixel[2]);

                            var dColor = pixel[2] + 256 * pixel[1] + 65536 * pixel[0];
                            if (dColor > 15) {
                                html5Colorpicker.find('.hexVal').val('#' + ('0000' + dColor.toString(16)).substr(-6));
                                html5Colorpicker.siblings("input[type='text']").val(('0000' + dColor.toString(16)).substr(-6));
                            }
                            else {
                                html5Colorpicker.find('.hexVal').val('#' + ('00000' + dColor.toString(16)).substr(-6));
                                html5Colorpicker.siblings("input[type='text']").val(('00000' + dColor.toString(16)).substr(-6));
                            }
                        }
                    });

                });

            }

        }
    };
    // auxiliar functions
    function hexToRgb(hex) {
        // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
        var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
        hex = hex.replace(shorthandRegex, function(m, r, g, b) {
            return r + r + g + g + b + b;
        });
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }
    function chooseSelector(path, index) {
        // choose desired color selector
        var imageSrc = path + '/images/colorwheel1.png';
        switch (index) {
            case '1':
                imageSrc = path + '/images/colorwheel2.png';
                break;
            case '2':
                imageSrc = path + '/images/colorwheel3.png';
                break;
            case '3':
                imageSrc = path + '/images/colorwheel4.png';
                break;
            case '4':
                imageSrc = path + '/images/colorwheel5.png';
                break;
            case '5':
                imageSrc = path + '/images/colorwheel6.png';
                break;
            case '6':
                imageSrc = path + '/images/colorwheel7.png';
                break;
            case '7':
                imageSrc = path + '/images/colorwheel8.png';
                break;
            case '8':
                imageSrc = path + '/images/colorpalette1.png';
                break;
            case '9':
                imageSrc = path + '/images/colorpalette2.png';
                break;
            case '10':
                imageSrc = path + '/images/colorpalette3.png';
                break;
            case '11':
                imageSrc = path + '/images/colorpalette4.png';
                break;
            case '12':
                imageSrc = path + '/images/colorpalette5.png';
                break;
            case '13':
                imageSrc = path + '/images/colorpalette6.png';
                break;
            case '14':
                imageSrc = path + '/images/colorpalette7.png';
                break;
            case '15':
                imageSrc = path + '/images/colorpalette8.png';
                break;
            case '16':
                imageSrc = path + '/images/colorspectrum1.png';
                break;
            case '17':
                imageSrc = path + '/images/colorspectrum2.png';
                break;
            case '18':
                imageSrc = path + '/images/colorspectrum3.png';
                break;
        }
        return imageSrc;
    }
})(jQuery);
