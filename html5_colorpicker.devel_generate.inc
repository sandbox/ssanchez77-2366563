<?php
/**
 * @file
 * Create random data to populate HTML5 colorpicker fields.
 */

/**
 * Implements hook_devel_generate().
 */
function html5_colorpicker_devel_generate($object, $field, $instance, $bundle) {
  if (field_behaviors_widget('multiple values', $instance) == FIELD_BEHAVIOR_CUSTOM) {
    return devel_generate_multiple('_html5_colorpicker_devel_generate', $object, $field, $instance, $bundle);
  }
  else {
    return _html5_colorpicker_devel_generate($object, $field, $instance, $bundle);
  }
}

function _html5_colorpicker_devel_generate($object, $field, $instance, $bundle) {
  $object_field['html5_colorpicker'] = dechex(rand(0, pow(255, 3) - 1));
  return $object_field;
}
