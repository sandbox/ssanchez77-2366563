-- SUMMARY --

The HTML5 Colorpicker creates a new Form API element type: html5_colorpicker. After enabling this module a new form element can be created and included in any form in this way:

<?php
$form['element'] = array(
  '#type' => 'html5_colorpicker',
  '#title' => t('Color'),
  '#default_value' => 'FFFFFF',
);
?>

This will automatically place the colorpicker into the form, allowing users of the form to choose a color.

This module includes Field API integration. The picker can be added as a field to any content type.

The HTML5 Colorpicker is an adaptation of the Drupal 7 Jaypan's Jquery Colorpicker, with the Script Tutorials' HTML5 Color Picker (canvas) code.

References:
  https://www.drupal.org/project/jquery_colorpicker
  http://www.script-tutorials.com/html5-color-picker-canvas/

-- REQUIREMENTS --

None.

-- INSTALLATION --

* Install as usual, see https://www.drupal.org/documentation/install/modules-themes/modules-7 for further information.

-- CONFIGURATION --

* Configure user permissions in People » Permissions:

  - Administer HTML5 colorpicker
    Allows users to administer the settings for the HTML5 colorpicker.

* Customize the settings in Configuration » Development » HTML5 Colorpicker.

-- CUSTOMIZATION --

* There are picker settings that can be override / customize:

  1) CONTROLS VISIBILITY

    Show / Hide RGB controls.
    Show / Hide HexValue control.

  2) WIDGET STYLE

    Border radius in pixels.
    Background Color, hex coded without the '#' character.
    Picker width in pixels, minimum recommended: 220.
    Picker height in pixels, minimum recommended: 220.
    Color Selector, to choose which color wheel, palette or spectrum will use.

-- CONTACT --

Current maintainers:
* Simon Sanchez Segura (ssanchez77) - https://www.drupal.org/user/2891869

This project has been sponsored by:
* CINETICA
  Think | Design | Build. Our company provides graphical solutions, both in launching and reinventing corporate identity. Aware of the current technological demand, our company provides customized website development. Visit http://www.cinetica.co.cr for more information.
